Use DataDict.pkg

Open OrderHeader
Open OrderDetail
Open Customer
Open SalesPerson
Open OrderSystem
Use sql.pkg

Register_Object Terms_table
Register_Object Ship_Table

Object Terms_table  is a DescriptionValidationTable
    
    Procedure Fill_List
        Forward Send Fill_List
        Send Add_Table_Value  "NONE"  "None established"
        Send Add_Table_Value  "COD"  "COD"
        Send Add_Table_Value  "NET30"  "Net 30"
        Send Add_Table_Value  "NET60"  "Net 60"
        Send Add_Table_Value  "NET90"  "Net 90"
        Send Add_Table_Value  "PREPAY"  "Pre-payment required"
    End_Procedure
    
End_Object

Object Ship_Table is a CodeValidationTable
    Set Type_Value to "SHIPPING"
    Set Allow_Blank_State to True
End_Object

Register_Object OrderHeader_sl
Register_Object oOrderWebLookup

// new DD class for sequences
Class cWSDataDictionary is a DataDictionary
    Procedure Construct_Object
        Forward Send Construct_Object

        // Used to store SEQUENCE field
        { Visibility=Private }
        Property Integer private.piSeqField 0
        
    End_Procedure

        { MethodType=Property }
        Procedure Set Field_SEQUENCE Integer iField Boolean bSet
            Set private.piSeqField to iField
        End_Procedure

        { Visibility=Private MethodType=Property NoDoc=True }
        Function Field_SEQUENCE Returns Integer
            Integer iField
            Get private.piSeqField to iField
            Function_Return iField
        End_Function

        {MethodType=Method NoDoc=True}
        // allocate a sequence from a SQL sequence object. 
        // Sequence naming std. is "seq_Tablename_FieldName"
        // The sequence needs to exist or a -1 is returned and error thrown
        Function Allocate_Sequence Integer iMain Integer iSeqField Returns Variant
            Integer iDriver iNumDrivers
            String sTableName sFieldName sSeqName
            String sSQL sValue
            String sCurrentDriver
            Variant vSequence
            Handle hoSQL hdbc hstmt
            Integer iResult
            
            // needed to get the root name of the table because of ALIASES and returns the driver in there
            Get_Attribute DF_NUMBER_DRIVERS to iNumDrivers
            For iDriver from 1 to iNumDrivers
                Get_Attribute DF_DRIVER_NAME of iDriver to sCurrentDriver
            Loop

            Get_Attribute DF_FILE_ROOT_NAME of iMain to sTableName

            // replace the driver in the root name of the table
            Move (Replace(sCurrentDriver,sTableName,"")) to sTableName
            Move (Replace(":",sTableName,""))            to sTableName

            Get_Attribute DF_FIELD_NAME of iMain iSeqField      to sFieldName
            Move (SFormat("SEQ_%1_%2",sTableName,sFieldName))   to sSeqName
            Move (SFormat("IF EXISTS (SELECT * FROM sys.sequences WHERE name = N'%1' AND schema_id = SCHEMA_ID(N'dbo')) BEGIN ",sSeqName)) to sSQL
            Append sSQL (SFormat("Select NEXT VALUE FOR [%1]",sSeqName))   
            Append sSQL "END ELSE BEGIN SELECT -1 END"
// DAW Driver 
            Get Create (RefClass(cSQLHandleManager))          to hoSQL
            Get SQLFileConnect of hoSQL iMain to hdbc 
            
            Get SQLOpen of hdbc to hstmt
            Send SQLExecDirect of hstmt sSQL    
            Get SQLFetch of hstmt to iResult
            If (iResult<>0) Begin 
                Get SQLColumnValue of hstmt 1 to vSequence
            End
            Send SQLClose of hstmt
            Send SQLDisconnect of hdbc
            Send Destroy of hoSQL

// Mertech driver - the CURSOR_TYPE TYPE_CLIENT is mandatory!
//            SQL_SET_STMT to sSQL
//            SQL_PREPARE_STMT CURSOR_TYPE TYPE_CLIENT
//            SQL_EXECUTE_STMT
//            SQL_FETCH_NEXT_ROW into vSequence
//            SQL_CANCEL_QUERY_STMT

            // the sequence does not exist. Create it and then allocate
            If (vSequence=-1) Begin 
                Send Create_Sequence iMain iSeqField
                Get Allocate_Sequence iMain iSeqField to vSequence
            End
            
            Function_Return vSequence
        End_Function

        Procedure Create_Sequence Integer iMain Integer iSeqField
            Integer iDriver iNumDrivers
            String sTableName sFieldName sSeqName
            String sSQL sValue
            String sCurrentDriver
            Integer iStart iMax iMaxValue iResult
            Handle hoSQL hstmt hdbc

            // needed to get the root name of the table because of ALIASES and returns the driver in there
            Get_Attribute DF_NUMBER_DRIVERS to iNumDrivers
            For iDriver from 1 to iNumDrivers
                Get_Attribute DF_DRIVER_NAME of iDriver to sCurrentDriver
            Loop

            Get_Attribute DF_FILE_ROOT_NAME of iMain to sTableName

            // replace the driver in the root name of the table
            Move (Replace(sCurrentDriver,sTableName,"")) to sTableName
            Move (Replace(":",sTableName,""))            to sTableName

            Get_Attribute DF_FIELD_NAME of iMain iSeqField to sFieldName
            
            // get the max of the fieldname from the table then increment by 1
            Move (SFormat("Select Max(%1) from %2",sFieldName,sTableName)) to sSQL
// DAW Driver
            Get Create (RefClass(cSQLHandleManager)) to hoSQL
            Get SQLFileConnect of hoSQL iMain        to hdbc 
            
            Get SQLOpen         of hdbc to hstmt
            Send SQLExecDirect  of hstmt sSQL    
            Get SQLFetch        of hstmt to iResult
            If (iResult<>0) Begin 
                Get SQLColumnValue of hstmt 1 to sValue
                Move (Integer(sValue)) to iStart
            End
            Send SQLClose of hstmt

// Mertech driver - the CURSOR_TYPE TYPE_CLIENT is mandatory!
//            SQL_SET_STMT to sSQL
//            SQL_PREPARE_STMT CURSOR_TYPE TYPE_CLIENT
//            SQL_EXECUTE_STMT
//            SQL_FETCH_NEXT_ROW into iStart
//            SQL_CANCEL_QUERY_STMT

            Increment iStart
            
            // ToDo: could maybe figure out what this is from DF?
            Move 2147483647 to iMaxValue
            Move (SFormat("SEQ_%1_%2 ",sTableName,sFieldName)) to sSeqName
            
            Move ("CREATE SEQUENCE " + sSeqName) to sSQL
            Append sSQL " start with "  (String(iStart)) " increment by 1 minvalue 1 maxvalue " (String(iMaxValue))

// Mertech driver - the CURSOR_TYPE TYPE_CLIENT is mandatory!
//            SQL_SET_STMT to sSQL
//            SQL_PREPARE_STMT CURSOR_TYPE TYPE_CLIENT
//            SQL_EXECUTE_STMT
//            SQL_CANCEL_QUERY_STMT

            Get SQLOpen         of hdbc to hstmt
            Send SQLExecDirect  of hstmt sSQL    
            Send SQLClose       of hstmt
            Send SQLDisconnect  of hdbc
            Send Destroy of hoSQL
            
        End_Procedure

        { MethodType=Event NoDoc=True }
        Procedure AutoIncrement
            Integer iMain iSeqField
            Variant vSequence
            
            Forward Send AutoIncrement
            
            Get Main_File to iMain
            
            Get Field_SEQUENCE to iSeqField
            If (iSeqField>0) Begin 
                Get Allocate_Sequence iMain iSeqField to vSequence
                If (vSequence=-1) Begin
                    Error 4145  
                End
                Else Begin 
                    Set_Field_Value iMain iSeqField to vSequence
                End
            End
        End_Procedure

End_Class

Class cOrderHeaderDataDictionary is a cWSDataDictionary
    
    Procedure Construct_Object
        Forward Send Construct_Object
        
        Set Main_File to OrderHeader.File_Number
        
        Set Foreign_Field_Option DD_KEYFIELD DD_FINDREQ to True
        Set Foreign_Field_Option DD_INDEXFIELD DD_NOPUT to True
        Set Foreign_Field_Option DD_DEFAULT DD_DISPLAYONLY to True
        
        Set Add_Client_File to OrderDetail.File_Number
        
        Set Add_Server_File to Customer.File_Number
        Set Add_Server_File to SalesPerson.File_Number
        
        Set ParentNullAllowed SalesPerson.File_Number to True
        Set ParentNoSwitchIfCommitted Customer.File_Number to True
        
        Set Add_System_File to OrderSystem.File_Number DD_LOCK_ON_NEW_SAVE_DELETE
        
        //Set Field_Auto_Increment Field OrderHeader.Order_Number to File_Field OrderSystem.Order_Number
        // replace above so using Field_Sequece to autoincrement 
        Set Field_SEQUENCE Field OrderHeader.Order_Number to True
        
        Set Field_Option Field OrderHeader.Order_Number DD_AUTOFIND to True
        Set Field_Prompt_Object Field OrderHeader.Order_Number to OrderHeader_SL
        Set Field_WebPrompt_Object Field OrderHeader.Order_Number to oOrderWebLookup
        
        Set Key_Field_State Field OrderHeader.Order_Number to True
        Set Status_Help Field OrderHeader.Order_Number to "Order Number - New orders are assigned numbers automatically"
        Set Field_Option Field OrderHeader.Order_Number DD_NOPUT to True
        
        Set Field_Class_Name Field OrderHeader.Order_Date to "Spin"
        Set Field_Entry_msg Field OrderHeader.Order_Date to Entry_Order_Date
        Set Field_Mask_Type Field OrderHeader.Order_Date to MASK_DATE_WINDOW
        Set Field_Prompt_Object Field OrderHeader.Order_Date to OrderHeader_SL
        Set Field_WebPrompt_Object Field OrderHeader.Order_Date to oOrderWebLookup
        
        Set Status_Help Field OrderHeader.Order_Date to "Date on which the order was placed"
        Set Field_Option Field OrderHeader.Order_Date DD_COMMIT to True
        Set Field_Option Field OrderHeader.Order_Date DD_REQUIRED to True
        
        Set Field_Class_Name Field OrderHeader.Terms to "Combo"
        Set Field_Value_Table Field OrderHeader.Terms to Terms_table
        Set Status_Help Field OrderHeader.Terms to "Payment terms"
        
        Set Field_Class_Name Field OrderHeader.Ship_Via to "Combo"
        Set Field_Value_Table Field OrderHeader.Ship_Via to Ship_Table
        Set Status_Help Field OrderHeader.Ship_Via to "Shipping method"
        
        Set Status_Help Field OrderHeader.Ordered_By to "Order placed by"
        Set Field_Option Field OrderHeader.Ordered_By DD_COMMIT to True
        
        Set Field_Label_Long Field OrderHeader.SalesPerson_ID to "Sales Person ID"
        Set Field_Label_Short Field OrderHeader.SalesPerson_ID to "Sales ID"
        Set Status_Help Field OrderHeader.SalesPerson_ID to "Sales Person who initiated the order"
        
        Set Field_Mask_Type Field OrderHeader.Order_Total to MASK_CURRENCY_WINDOW
        Set Field_Option Field OrderHeader.Order_Total DD_DISPLAYONLY to True
        
        Set piPrimaryIndex to 1
    End_Procedure
    
    Procedure Field_Defaults
        Forward Send Field_Defaults
        Set Field_Changed_Value Field OrderHeader.Order_Date to (CurrentDateTime())
        Set Field_Changed_Value Field OrderHeader.Terms to "COD"
        Set Field_Changed_Value Field OrderHeader.Ship_Via to "FEDEX"
    End_Procedure
    
    // Add a default date if the field is blank
    Procedure Entry_Order_Date Integer iField Date dDate
        Integer iChanged
        
        Get Field_Changed_State iField to iChanged
        If (iChanged=0 and dDate=0) Begin
            Sysdate dDate
            Set Field_Default_Value iField to dDate
        End
    End_Procedure
    
    Procedure Update
        Forward Send Update
        Send Adjust_Balances OrderHeader.Order_Total
    End_Procedure
    
    Procedure Backout
        Forward Send Backout
        Send Adjust_Balances (-OrderHeader.Order_Total)
    End_Procedure
    
    Procedure Adjust_Balances Number Amt
        Add Amt to Customer.Purchases
        Add Amt to Customer.Balance
    End_Procedure
    
    Procedure Deleting
        Forward Send Deleting
        // see if we can decrement the order number in sys table...
        // can only do this if this is the newest order.
        If (OrderHeader.Order_Number=OrderSystem.Order_Number) Begin // if this is
            Decrement OrderSystem.Order_Number          // the last number,
            SaveRecord OrderSystem                      // decrement and save.
        End
    End_Procedure
    
End_Class

#IFDEF Is$WebApp

Use OrderWebLookup.wo

#ELSE

Use OrderHeader.sl

#ENDIF
